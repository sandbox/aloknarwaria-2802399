<?php

namespace Drupal\domain_gtm;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class MultiDomainGTMService.
 *
 * @package Drupal\multi_domain_gtm
 */
class DomainGTMService implements EventSubscriberInterface {

  /**
   * Constructor.
   */
  public function __construct() {
    
  }

  function update_multi_domain_gtm_container_id() {
    /*
     * load active domain keys
     */
    $negotiator = \Drupal::service('domain.negotiator');
    $domain = $negotiator->getActiveDomain();
    if (is_array($domain)) {
      if (null !== $domain->id()) {
        $configFactory = \Drupal::config('google_tag.settings');
        $container_id = $configFactory->get($domain->id());
        // Check for container id is set for current active domain.
        if (isset($container_id)) {
          $configFactory->setSettingsOverride(array('container_id' => $container_id));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('update_multi_domain_gtm_container_id');
    return $events;
  }

}
